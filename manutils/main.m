//
//  main.m
//  manutils
//
//  Created by macpps on 2019/2/20.
//  Copyright © 2019 PHN. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
