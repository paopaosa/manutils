//
//  ViewController.m
//  manutils
//
//  Created by macpps on 2019/2/20.
//  Copyright © 2019 PHN. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)viewWillAppear {
    [super viewWillAppear];
    self.view.window.title = @"我的工具集";
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
